from bs4 import BeautifulSoup
from bs4.element import NavigableString, Tag
import sys, os

def equals(x, y):
    """
    :type x: bs4.element.NavigableString
    :type y: bs4.element.NavigableString
    """

    def f(t1, t2):
        #print 'a={}, b={}'.format(t1, t2)
        return t1 != None and t2 != None and t1.name == t2.name

    print map(f, x.find_all_next(), y.find_all_next())

    return reduce(lambda x, y: x and y,
        map(f, x.find_all_next(), y.find_all_next()))

def traverse(source, target):

    for s, t in zip(source, target):

        #print type(s), type(t)
        if isinstance(s, NavigableString) and isinstance(t, NavigableString):
            s, t = unicode(s).strip(), unicode(t).strip()

            if len(s) > 0 and len(t) > 0:
                yield s, t

        else:
            s, t = s.find_next(), t.find_next()

            if s.name == t.name and s.attrs == t.attrs and len(s.contents) == len(t.contents):
                traverse(s.descendants, t.descendants)


if __name__ == '__main__':
    source = BeautifulSoup(open('sample-en-us.html').read())
    target = BeautifulSoup(open('sample-ko-kr.html').read())

    for x in traverse(source.find(id='content').descendants, target.find(id='content').descendants):
        print x[0], '<<>>', x[1]
        print '-----------------'

