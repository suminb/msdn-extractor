import requests

if __name__ == '__main__':
    languages = ('en-us', 'ko-kr')
    #url = 'http://msdn.microsoft.com/{}/library/windows/apps/br211375.aspx'
    url = 'http://msdn.microsoft.com/{}/library/windows/apps/jj945493.aspx'

    for language in languages:
        req = requests.get(url.format(language))

        with open('sample-{}.html'.format(language), 'w') as f:
            f.write(req.text.encode('utf-8'))